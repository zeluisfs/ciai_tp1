/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports) {

	"use strict";
	
	var _obj;
	
	var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };
	
	var _ReactRouter = ReactRouter,
	    Router = _ReactRouter.Router,
	    Route = _ReactRouter.Route,
	    IndexRoute = _ReactRouter.IndexRoute,
	    Redirect = _ReactRouter.Redirect,
	    Link = _ReactRouter.Link,
	    IndexLink = _ReactRouter.IndexLink,
	    browserHistory = _ReactRouter.browserHistory;
	
	// = = = = = = = = [MAIN_LAYOUT]
	
	var MainLayout = React.createClass({
		displayName: "MainLayout",
	
		render: function render() {
			return React.createElement(
				"div",
				null,
				React.createElement(Header, null),
				React.createElement(
					"main",
					null,
					this.props.children
				),
				React.createElement(SignInForm, null),
				React.createElement(Footer, null)
			);
		}
	});
	
	// Header react class, should contain
	// FCT logo along with the navbar
	// and the login / register components
	var Header = React.createClass({
		displayName: "Header",
	
		componentDidMount: function componentDidMount() {
			$("#nav-sign-in").click(function () {
				$("#sign-in").slideToggle();
			});
		},
	
		render: function render() {
			return React.createElement(
				"div",
				null,
				React.createElement(
					"header",
					{ className: "container-fluid row" },
					React.createElement("img", { id: "logo", src: "images/logo_fct_w.png", className: "img-responsive hidden-xs col-sm-4" }),
					React.createElement(
						"nav",
						{ id: "main-navigation", className: "navbar-right" },
						React.createElement(
							Link,
							{ to: "/" },
							"Home"
						),
						React.createElement(
							Link,
							{ to: "/profile" },
							"Student"
						),
						React.createElement(
							Link,
							{ id: "/professor" },
							"Professor"
						),
						React.createElement(
							Link,
							{ to: "/degrees" },
							"Degrees"
						),
						React.createElement(
							Link,
							{ to: "/courses" },
							"Courses"
						),
						React.createElement(
							"span",
							null,
							React.createElement(
								"button",
								{ id: "nav-sign-in", className: "btn btn-primary" },
								"Sign In"
							)
						)
					)
				),
				React.createElement("div", { id: "alert-box", className: "alert" })
			);
		}
	
	});
	
	var ContactUs = React.createClass({
		displayName: "ContactUs",
	
		render: function render() {
			return React.createElement(
				"div",
				{ id: "contact-us", className: "row" },
				React.createElement(
					"div",
					{ className: "col-sm-8" },
					"TODO"
				),
				React.createElement(
					"address",
					{ className: "col-sm-4 address" },
					"TODO"
				)
			);
		}
	});
	
	var SignInForm = React.createClass({
		displayName: "SignInForm",
	
		render: function render() {
			return React.createElement(
				"div",
				{ id: "sign-in", className: "container span12" },
				React.createElement(
					"div",
					{ className: "login well well-small row" },
					React.createElement(
						"form",
						null,
						React.createElement(
							"div",
							{ className: "input-group" },
							React.createElement(
								"span",
								{ className: "input-group-addon" },
								React.createElement("i", { className: "glyphicon glyphicon-user" })
							),
							React.createElement("input", { className: "form-control", type: "text", required: "required", placeholder: "Username" })
						),
						React.createElement(
							"div",
							{ className: "input-group" },
							React.createElement(
								"span",
								{ className: "input-group-addon" },
								React.createElement("i", { className: "glyphicon glyphicon-lock" })
							),
							React.createElement("input", { className: "form-control", type: "password", required: "required", placeholder: "Password" })
						),
						React.createElement(
							"button",
							{ onClick: this.handleSubmit, className: "btn btn-primary" },
							"Sign In"
						)
					)
				)
			);
		},
	
		handleSubmit: function handleSubmit(e) {
			// TODO, depends on success or failure of logging in
			e.preventDefault();
			$('#sign-in').hide();
			$('#alert-box').addClass("alert-success").html("<b>Success!</b> Log In attempt has succeeded. <a class='close' data-dismiss='alert' aria-label='close'>&times;</a>").slideDown();
		}
	});
	
	var Home = React.createClass({
		displayName: "Home",
	
		render: function render() {
			return React.createElement(
				"div",
				{ className: "container-fluid" },
				React.createElement("div", { id: "banner", className: "img-responsive" }),
				React.createElement(
					"h3",
					{ className: "onImageMessage text-center" },
					"Join the best University in the country"
				),
				React.createElement(DegreeIndex, null),
				React.createElement(ContactUs, null)
			);
		}
	});
	
	// Footer react class, should show the copyright
	// as well as year and whatnot
	var Footer = React.createClass({
		displayName: "Footer",
	
		render: function render() {
			return React.createElement(
				"footer",
				{ className: "container-fluid row text-center" },
				React.createElement(
					"p",
					{ className: "col-sm-4" },
					"2016"
				),
				React.createElement(
					"p",
					{ className: "col-sm-4" },
					"Copyright \xA9"
				),
				React.createElement(
					"p",
					{ className: "col-sm-4" },
					"FCT-UNL"
				)
			);
		}
	});
	
	//= = = = = = = = [DEGREE]
	var DegreeIndex = React.createClass({
		displayName: "DegreeIndex",
	
		getInitialState: function getInitialState() {
			return {
				listDegrees: []
			};
		},
	
		componentDidMount: function componentDidMount() {
			$.ajax({
				type: "GET",
				url: "http://localhost:8080/degrees",
				success: function (response) {
					this.setState({ listDegrees: response });
				}.bind(this),
				error: function error(e) {
					console.log("error", e);
				}
			});
		},
	
		render: function render() {
			if (this.state.listDegrees == null) {
				return React.createElement("div", null);
			} else {
				return React.createElement(
					"table",
					{ className: "table table-responsive table-striped container table-hover table-inverse" },
					React.createElement(
						"thead",
						null,
						React.createElement(
							"tr",
							null,
							React.createElement(
								"td",
								null,
								"Name"
							),
							React.createElement(
								"td",
								null,
								"Years"
							),
							React.createElement(
								"td",
								null,
								"ECTS"
							)
						)
					),
					React.createElement(
						"tbody",
						null,
						this.state.listDegrees.map(function (deg) {
							return React.createElement(
								"tr",
								{ key: deg.name },
								React.createElement(
									"td",
									null,
									React.createElement(
										Link,
										{ to: "degree/".concat(deg.id) },
										deg.name
									)
								),
								React.createElement(
									"td",
									null,
									deg.years
								),
								React.createElement(
									"td",
									null,
									deg.ects
								)
							);
						})
					)
				);
			}
		}
	});
	
	//= = = = = = = = [STUDENT]
	var StudentLayout = React.createClass({
		displayName: "StudentLayout",
	
		getInitialState: function getInitialState() {
			return { student: {} };
		},
		componentDidMount: function componentDidMount() {
			$.ajax({
				type: "GET",
				url: "http://localhost:8080/api/students/1",
				success: function (response) {
					this.setState({ student: response });
				}.bind(this),
				error: function error(e) {
					console.log("error", e);
				}
			});
		},
		render: function render() {
			return React.createElement(
				"div",
				null,
				this.state.student != null && React.createElement(
					"div",
					null,
					React.createElement(StudentProfile, { student: this.state.student, handleChange: this.handleChange }),
					React.createElement("hr", null),
					React.createElement(Enrolled, { enrolled: this.state.student.enrolledCourses }),
					React.createElement("hr", null),
					React.createElement(Concluded, { concluded: this.state.student.concludedCourses })
				)
			);
		},
	
		handleChange: function handleChange(e) {
			e.preventDefault();
			var value = e.target.value;
			if (!(value == null)) {
				if (e.target.id == "address-input") {
					this.setState(function (prevState) {
						var ns = prevState.student;
						ns.address = value;
						return { student: ns };
					});
				} else if (e.target.id == "email-input") {
					this.setState(function (prevState) {
						var ns = prevState.student;
						ns.email = value;
						return { student: ns };
					});
				} else if (e.target.id == "photo-input") {
					this.setState(function (prevState) {
						var ns = prevState.student;
						ns.photo = value;
						return { student: ns };
					});
				}
			}
		}
	});
	
	var Enrolled = React.createClass({
		displayName: "Enrolled",
	
		render: function render() {
			if (this.props.enrolled == null) {
				return React.createElement("div", null);
			} else {
				return React.createElement(
					"table",
					{ className: "table table-responsive table-striped container table-hover table-inverse" },
					React.createElement(
						"caption",
						null,
						"Enrolled"
					),
					React.createElement(
						"thead",
						null,
						React.createElement(
							"tr",
							null,
							React.createElement(
								"td",
								null,
								"Id"
							),
							React.createElement(
								"td",
								null,
								"Edition"
							),
							React.createElement(
								"td",
								null,
								"Name"
							),
							React.createElement(
								"td",
								null,
								"ECTS"
							)
						)
					),
					React.createElement(
						"tbody",
						null,
						this.props.enrolled.map(function (course) {
							return React.createElement(
								"tr",
								{ key: course.id },
								React.createElement(
									"td",
									null,
									course.id
								),
								React.createElement(
									"td",
									null,
									course.edition
								),
								React.createElement(
									"td",
									null,
									React.createElement(
										Link,
										{ to: "/courses" },
										course.name
									)
								),
								React.createElement(
									"td",
									null,
									course.ECTS
								)
							);
						})
					)
				);
			}
		}
	});
	
	var Concluded = React.createClass({
		displayName: "Concluded",
	
		render: function render() {
			if (this.props.concluded == null) {
				return React.createElement("div", null);
			} else {
				return React.createElement(
					"table",
					{ className: "table table-responsive table-striped container table-hover table-inverse" },
					React.createElement(
						"caption",
						null,
						"Concluded"
					),
					React.createElement(
						"thead",
						null,
						React.createElement(
							"tr",
							null,
							React.createElement(
								"td",
								null,
								"Id"
							),
							React.createElement(
								"td",
								null,
								"Edition"
							),
							React.createElement(
								"td",
								null,
								"Name"
							),
							React.createElement(
								"td",
								null,
								"ECTS"
							)
						)
					),
					React.createElement(
						"tbody",
						null,
						this.props.concluded.map(function (course) {
							return React.createElement(
								"tr",
								{ key: course.id },
								React.createElement(
									"td",
									null,
									course.id
								),
								React.createElement(
									"td",
									null,
									course.edition
								),
								React.createElement(
									"td",
									null,
									React.createElement(
										Link,
										{ to: "/courses" },
										course.name
									)
								),
								React.createElement(
									"td",
									null,
									course.ECTS
								)
							);
						})
					)
				);
			}
		}
	});
	
	var StudentProfile = React.createClass(_obj = {
		displayName: "StudentProfile",
	
		render: function render() {
			return React.createElement(
				"div",
				{ id: "profile", className: "container center-block" },
				React.createElement(
					"div",
					{ className: "col-sm-4" },
					React.createElement("img", { src: this.props.student.photo, alt: "user_img", className: "img-responsive img-circle" }),
					React.createElement("span", { onClick: this.editInfo, id: "photoSubmit", className: "glyphicon glyphicon-wrench" }),
					React.createElement(
						"div",
						{ id: "photo-input-div", className: "input-group well" },
						React.createElement("input", { id: "photo-input", type: "text", onChange: this.props.handleChange, onBlur: this.saveChanges, value: this.props.student.photo })
					)
				),
				React.createElement(
					"div",
					{ className: "col-sm-8" },
					React.createElement(
						"form",
						null,
						React.createElement(
							"table",
							{ className: "table table-responsive table-striped" },
							React.createElement(
								"tbody",
								null,
								React.createElement(
									"tr",
									null,
									React.createElement(
										"td",
										null,
										React.createElement(
											"b",
											null,
											"Name:"
										)
									),
									React.createElement(
										"td",
										null,
										this.props.student.name
									)
								),
								React.createElement(
									"tr",
									null,
									React.createElement(
										"td",
										null,
										React.createElement(
											"b",
											null,
											"Number:"
										)
									),
									React.createElement(
										"td",
										null,
										this.props.student.id
									)
								),
								React.createElement(
									"tr",
									null,
									React.createElement(
										"td",
										null,
										React.createElement(
											"b",
											null,
											"Email:"
										)
									),
									React.createElement(
										"td",
										null,
										React.createElement("input", { id: "email-input", required: "required", type: "text", onChange: this.props.handleChange, value: this.props.student.email }),
										React.createElement("span", { onClick: this.editInfo, id: "emailSubmit", className: "glyphicon glyphicon-wrench" })
									)
								),
								React.createElement(
									"tr",
									null,
									React.createElement(
										"td",
										null,
										React.createElement(
											"b",
											null,
											"Address:"
										)
									),
									React.createElement(
										"td",
										null,
										React.createElement("input", { id: "address-input", type: "text", onChange: this.props.handleChange, value: this.props.student.address }),
										React.createElement("span", { onClick: this.editInfo, id: "addressSubmit", className: "glyphicon glyphicon-wrench" })
									)
								),
								React.createElement(
									"tr",
									null,
									React.createElement(
										"td",
										null,
										React.createElement(
											"b",
											null,
											"Degree:"
										)
									),
									React.createElement(
										"td",
										null,
										this.props.student.degree
									)
								),
								React.createElement(
									"tr",
									null,
									React.createElement(
										"td",
										null,
										React.createElement(
											"b",
											null,
											"Average:"
										)
									),
									React.createElement(
										"td",
										null,
										this.props.student.average
									)
								)
							)
						)
					)
				)
			);
		},
	
		editInfo: function editInfo(e) {
			e.preventDefault();
	
			if (e.target.id == "emailSubmit") {
				$("#email-input").focus();
			} else if (e.target.id == "addressSubmit") {
				$("#address-input").focus();
			} else if (e.target.id == "photoSubmit") {
				$('#photo-input-div').show();
				$('#photo-input-div').focus();
			}
		},
		saveChanges: function saveChanges(e) {
			e.preventDefault();
	
			// TODO
			if (e.target.id == "address-input") {} else if (e.target.id == "email-input") {} else if (e.target.id == "photo-input") {
				$("#photo-input-div").hide();
			}
		},
		handleChange: function handleChange(e) {
			e.preventDefault();
			var value = e.target.value;
			if (!(value == null)) {
				if (e.target.id == "address-input") {
					_get(_obj.__proto__ || Object.getPrototypeOf(_obj), "setState", this).call(this, function (prevState) {
						var ns = prevState.student;
						ns.address = value;
						return { student: ns };
					});
				} else if (e.target.id == "email-input") {
					_get(_obj.__proto__ || Object.getPrototypeOf(_obj), "setState", this).call(this, function (prevState) {
						var ns = prevState.student;
						ns.email = value;
						return { student: ns };
					});
				} else if (e.target.id == "photo-input") {
					_get(_obj.__proto__ || Object.getPrototypeOf(_obj), "setState", this).call(this, function (prevState) {
						var ns = prevState.student;
						ns.photo = value;
						return { student: ns };
					});
				}
			}
		}
	});
	
	//= = = = = = = = [COURSES]
	var CourseDetails = React.createClass({
		displayName: "CourseDetails",
	
		render: function render() {
			return React.createElement(
				"div",
				null,
				this.props.params.course
			);
		}
	});
	
	var CourseIndex = React.createClass({
		displayName: "CourseIndex",
	
		render: function render() {
			return React.createElement(
				"table",
				{ className: "table table-responsive table-striped container table-hover table-inverse" },
				React.createElement(
					"caption",
					null,
					"All Available Courses"
				),
				React.createElement(
					"thead",
					null,
					React.createElement(
						"tr",
						null,
						React.createElement(
							"td",
							null,
							"Id"
						),
						React.createElement(
							"td",
							null,
							"Edition"
						),
						React.createElement(
							"td",
							null,
							"Name"
						),
						React.createElement(
							"td",
							null,
							"ECTS"
						)
					)
				),
				React.createElement(
					"tbody",
					null,
					this.props.courses.map(function (course) {
						return React.createElement(
							"tr",
							{ key: course.id },
							React.createElement(
								"td",
								null,
								course.id
							),
							React.createElement(
								"td",
								null,
								course.edition
							),
							React.createElement(
								"td",
								null,
								React.createElement(
									Link,
									{ to: "/courses/".concat(course.id) },
									course.name
								)
							),
							React.createElement(
								"td",
								null,
								course.ECTS
							)
						);
					})
				)
			);
		}
	});
	
	var CourseLayout = React.createClass({
		displayName: "CourseLayout",
	
		getInitialState: function getInitialState() {
			return {
				course: {},
				courses: []
			};
		},
	
		componentDidMount: function componentDidMount() {
			$.ajax({
				type: "GET",
				url: "http://localhost:8080/api/courses/",
				success: function (response) {
					this.setState({ courses: response });
				}.bind(this),
				error: function error(e) {
					console.log("error", e);
				}
			});
	
			$.ajax({
				type: "GET",
				url: "/assets/json/singleCourseDetails.json",
				success: function (response) {
					this.setState({ course: response });
				}.bind(this),
				error: function error(e) {
					console.log("error", e);
				}
			});
		},
	
		render: function render() {
			return React.createElement(
				"div",
				null,
				React.createElement(CourseDetails, { course: this.props.params.courseId }),
				React.createElement(CourseIndex, { courses: this.state.courses })
			);
		}
	});
	
	//** *** ==================== ROUTER ===================== ***// */
	ReactDOM.render(React.createElement(
		Router,
		null,
		React.createElement(
			Route,
			{ component: MainLayout },
			React.createElement(Route, { path: "/", component: Home }),
			React.createElement(Route, { path: "/profile", component: StudentLayout }),
			React.createElement(Route, { path: "/profile", component: StudentLayout }),
			React.createElement(Route, { path: "/degrees", component: DegreeIndex }),
			React.createElement(Route, { path: "/courses/:courseId", component: CourseLayout }),
			React.createElement(Route, { path: "/courses", component: CourseLayout })
		)
	), document.getElementById('react_content'));

/***/ }
/******/ ]);
//# sourceMappingURL=bundle.js.map
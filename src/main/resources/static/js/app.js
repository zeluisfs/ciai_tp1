const {Router,Route,IndexRoute,Redirect,Link,IndexLink,browserHistory} = ReactRouter

// = = = = = = = = [MAIN_LAYOUT]
var MainLayout = React.createClass({
	render: function(){
		return(
			<div>
				<Header />
				<main>{this.props.children}</main>
				<SignInForm />
				<Footer />
			</div>
		)
	}
});

// Header react class, should contain
// FCT logo along with the navbar
// and the login / register components
var Header = React.createClass({
		componentDidMount: function(){
			$("#nav-sign-in").click(function(){
				$("#sign-in").slideToggle();
			});
		},
	
		render: function(){
		return (
			<div>
				<header className='container-fluid row'>
					<img id='logo' src="images/logo_fct_w.png" className='img-responsive hidden-xs col-sm-4' />
					<nav id='main-navigation' className='navbar-right'>
						<Link to='/'>Home</Link>
						<Link to='/profile'>Student</Link>
						<Link to='/professor'>Professor</Link>
						<Link to='/degrees'>Degrees</Link>
						<Link to='/courses'>Courses</Link>
						<span><button id='nav-sign-in' className='btn btn-primary'>Sign In</button></span>
					</nav>
				</header>
				<div id='alert-box' className='alert'>
					
				</div>
			</div>
		)
	}

});

var ContactUs = React.createClass({
	render: function(){
		return(
			<div id='contact-us' className='row'>
				<div className='col-sm-8'>TODO</div>
				<address className='col-sm-4 address'>TODO</address>
			</div>
		)
	}
});

var SignInForm = React.createClass({
	render: function(){
		return(
			<div id='sign-in' className='container span12'>
				<div className='login well well-small row'>
					<form>
						<div className='input-group'>
							<span className='input-group-addon'><i className='glyphicon glyphicon-user'></i></span>
							<input className='form-control' type='text' required='required' placeholder='Username'></input>
						</div>
						<div className='input-group'>
							<span className='input-group-addon'><i className='glyphicon glyphicon-lock'></i></span>
							<input className='form-control' type='password' required='required' placeholder='Password'></input>
						</div>
						<button onClick={this.handleSubmit} className="btn btn-primary">Sign In</button>
					</form>
				</div>
			</div>
		)
	},
	
	handleSubmit(e){ // TODO, depends on success or failure of logging in
		e.preventDefault();
		$('#sign-in').hide();
		$('#alert-box').addClass("alert-success")
		.html("<b>Success!</b> Log In attempt has succeeded. <a class='close' data-dismiss='alert' aria-label='close'>&times;</a>").slideDown();
	}
});

var Home = React.createClass({
	render: function(){
		return(
			<div className='container-fluid'>
				<div id='banner' className='img-responsive'></div>
				<h3 className='onImageMessage text-center'>Join the best University in the country</h3>
				<DegreeIndex />
				<ContactUs />
			</div>
		)
	}
})

// Footer react class, should show the copyright
// as well as year and whatnot
var Footer = React.createClass({
	render: function(){
		return (
			<footer className='container-fluid row text-center'>
		        <p className='col-sm-4'>2016</p>
		        <p className='col-sm-4'>Copyright &copy;</p>
		        <p className='col-sm-4'>FCT-UNL</p>
		    </footer>		
		)
	}
});



// = = = = = = = = [DEGREE]
var DegreeIndex = React.createClass({
	getInitialState: function() {
		return {
			listDegrees: []
		}
	},
	
	componentDidMount: function(){
		$.ajax({
			type: "GET",
			url: "/api/degrees",
			success: function(response) {
				this.setState({listDegrees: response._embedded.degrees});
			}.bind(this),
			error: function(e) {
				console.log("error", e);
			}
		});
	},
	
	render: function(){	
		if(this.state.listDegrees == null){
			return (<div></div>);
		}else{
			return(		
				<table className='table table-responsive table-striped container table-hover table-inverse'>
					<thead>
						<tr>
							<td>Name</td>
							<td>Years</td>
							<td>ECTS</td>
						</tr>
					</thead>
					<tbody>
						{this.state.listDegrees.map(deg => (
							<tr key={deg.name}>
								<td><Link to={"degree/".concat(deg.id)}>{deg.name}</Link></td>
								<td>{deg.years}</td>
								<td>{deg.ects}</td>
							</tr>
						))}
					</tbody>
				</table>
			)
		}
	}
});

// = = = = = = = = [STUDENT]
var StudentLayout = React.createClass({
	getInitialState: function(){
		return { 
			student: [],
			degree: [],
			enrolled: [],
			concluded: []
		}
	},
	componentDidMount: function(){
		$.ajax({
			type: "GET",
			url: "/api/students/1",
			success: function(response) {
				this.setState({student: response});
			}.bind(this),
			error: function(e) {
				console.log("error", e);
			}
		});
		
		$.ajax({
			type: "GET",
			url: "/api/students/1/degree",
			success: function(response) {
				this.setState({degree: response});
			}.bind(this),
			error: function(e) {
				console.log("error", e);
			}
		});
		
		$.ajax({
			type: "GET",
			url: "/api/students/1/enrolled",
			success: function(response) {
				this.setState({enrolled: response._embedded.courses});
			}.bind(this),
			error: function(e) {
				console.log("error", e);
			}
		});
		$.ajax({
			type: "GET",
			url: "/api/students/1/concluded",
			success: function(response) {
				this.setState({concluded: response._embedded.courses});
			}.bind(this),
			error: function(e) {
				console.log("error", e);
			}
		});
	},
	render: function(){
		return (
			<div>
				{this.state.student!=null &&
					<div>
						<StudentProfile student={this.state.student} degree={this.state.degree} handleChange={this.handleChange}/>
						<hr />
						<Enrolled enrolled={this.state.enrolled} />
						<hr />
						<Concluded concluded={this.state.concluded} />				
					</div>
				}			
			</div>
		)
	},
	
	handleChange(e){
		e.preventDefault();
		var value = e.target.value
		if(!(value == null)){
			if(e.target.id == "address-input"){
				this.setState(
					function(prevState) {
						var ns = prevState.student
						ns.address = value;
						return {student:ns}
					}
				)	
			}else if(e.target.id == "email-input"){
				this.setState(
						function(prevState) {
							var ns = prevState.student
							ns.email = value;
							return {student:ns}
						}
					)
			}else if(e.target.id == "photo-input"){
				this.setState(
						function(prevState) {
							var ns = prevState.student
							ns.photo = value;
							return {student:ns}
						}
					)
			}
		}	
	}
});

var Enrolled = React.createClass({	
	render: function(){
		if(this.props.enrolled==null){
			return(<div></div>);
		}else{
			return(
				<table className='table table-responsive table-striped container table-hover table-inverse'>
					<caption>Enrolled</caption>
					<thead>
						<tr>
							<td>Name</td>
							<td>Description</td>
							<td>ECTS</td>
						</tr>
					</thead>
					<tbody>
						{this.props.enrolled.map(course => (
							<tr key={course._links.self.href}>
								<td><Link to={course._links.self.href}>{course.name}</Link></td>
								<td>{course.description}</td>
								<td>{course.ects}</td>
							</tr>
						))}
					</tbody>
				</table>
			)
		}
	}
});

var Concluded = React.createClass({	
	render: function(){
		if(this.props.concluded==null){
			return(<div></div>);
		}else{
			return(
				<table className='table table-responsive table-striped container table-hover table-inverse'>
					<caption>Concluded</caption>
					<thead>
						<tr>
						<td>Name</td>
						<td>Description</td>
						<td>ECTS</td>
						</tr>
					</thead>
					<tbody>
						{this.props.concluded.map(course => (
							<tr key={course._links.self.href}>
								<td><Link to={course._links.self.href}>{course.name}</Link></td>
								<td>{course.description}</td>
								<td>{course.ects}</td>
							</tr>
						))}
					</tbody>
				</table>
			)
		}
	}
});


var StudentProfile = React.createClass({
	render: function(){
		return(
			<div id='profile' className='container center-block'>
				<div className='col-sm-4'>
					<img src={this.props.student.photo} alt='user_img' className='img-responsive img-circle'/>
					<span onClick={this.editInfo} id="photoSubmit" className="glyphicon glyphicon-wrench"></span>
					<div id='photo-input-div' className='input-group well'>
						<input id='photo-input' type='text' onChange={this.props.handleChange} onBlur={this.saveChanges} value={this.props.student.photo} />
					</div>
				</div>
				<div className='col-sm-8'>
					<form>
						<table className='table table-responsive table-striped'>
							<tbody>
								<tr>
									<td><b>Name:</b></td>
									<td>{this.props.student.name}</td>
								</tr>
								<tr>
									<td><b>Number:</b></td>
									<td>{this.props.student.id}</td>
								</tr>				
								<tr>
									<td><b>Email:</b></td>
									<td><input id='email-input' required="required" type="text" onChange={this.props.handleChange} value={this.props.student.email}/>
									<span onClick={this.editInfo} id="emailSubmit" onBlur={this.saveChanges} className="glyphicon glyphicon-wrench"></span>
									</td>
									
								</tr>
								<tr>
									<td><b>Address:</b></td>
									<td><input id='address-input' type="text" onChange={this.props.handleChange} value={this.props.student.address} />
									<span onClick={this.editInfo} id="addressSubmit" onBlur={this.saveChanges} className="glyphicon glyphicon-wrench"></span>
									</td>
								</tr>
								<tr>
									<td><b>Degree:</b></td>
									<td>{this.props.degree.name}</td>
								</tr>
								<tr>
									<td><b>Average:</b></td>
									<td>{this.props.student.average}</td>
								</tr>
							</tbody>
						</table>
					</form>
				</div>
			</div>
		)
	},
	
	editInfo(e){
		e.preventDefault();
	
		if(e.target.id == "emailSubmit"){
			$("#email-input").focus();
		}else if (e.target.id == "addressSubmit"){
			$("#address-input").focus();
		}else if(e.target.id == "photoSubmit"){	
			$('#photo-input-div').show();
			$('#photo-input-div').focus();
		}	
	},
	
	saveChanges(e){
		e.preventDefault();
		$("#photo-input-div").hide();
		
		
		var newValues = {
				"name":this.props.student.name,
				"email":this.props.student.email,
				"address":this.props.student.address,
				"photo":this.props.student.photo
		};
		
		$.ajax({
			  type: "PUT",
			  url: "/api/students/1",
			  data: JSON.stringify(newValues),
			  success: function(){},
		      error: function () {},
			  dataType: "json",
			  contentType : "application/json"
		});
		
	},
	
	handleChange(e){
		e.preventDefault();
		var value = e.target.value
		if(!(value == null)){
			if(e.target.id == "address-input"){
				super.setState(
					function(prevState) {
						var ns = prevState.student
						ns.address = value;
						return {student:ns}
					}
				)	
			}else if(e.target.id == "email-input"){
				super.setState(
						function(prevState) {
							var ns = prevState.student
							ns.email = value;
							return {student:ns}
						}
					)
			}else if(e.target.id == "photo-input"){
				super.setState(
						function(prevState) {
							var ns = prevState.student
							ns.photo = value;
							return {student:ns}
						}
					)
			}
		}	
	},
});

/// /= = = = = = = = [COURSE] 
var CourseIndex = React.createClass({
	render: function(){
		
		return (
			<table className='table table-responsive table-striped container table-hover table-inverse'>
				<caption>All Available Courses</caption>
				<thead>
					<tr>
						<td>Id</td>
						<td>Name</td>
						<td>Description</td>
						<td>ECTS</td>
					</tr>
				</thead>
				<tbody>
					{this.props.courses.map(course => (
						<tr key={course._links.self.href}>
							<td>{course.id}</td>
							<td><Link to={course._links.self.href}>{course.name}</Link></td>
							<td>{course.description}</td>
							<td>{course.ects}</td>
						</tr>
					))}
				</tbody>
			</table>
		)
	}
});

var CourseLayout = React.createClass({
	getInitialState: function(){
		return { 
			listCourses:[]  
		}
	},
	
	componentDidMount: function(){
		$.ajax({
			type: "GET",
			url: "/api/courses/",
			success: function(response) {
				this.setState({listCourses: response._embedded.courses});
			}.bind(this),
			error: function(e) {
				console.log("error", e);
			}
		});
		
	},
	
	render: function(){
		return (
			<div>
				<CourseIndex courses={this.state.listCourses} />
			</div>
		)
	}
});

var CourseDetails = React.createClass({
	getInitialState: function(){
		return { 
			course: {}
		}
	},
	
	componentDidMount: function(){
		$.ajax({
			type: "GET",
			url: "/api/courses/".concat(this.props.params.courseId),
			success: function(response) {
				this.setState({course: response});
			}.bind(this),
			error: function(e) {
				console.log("error", e);
			}
		});
		
	},
	
	render: function(){
		return(
			<table className='table table-responsive table-striped container table-hover table-inverse'>
				<caption>Course Details</caption>
				<thead>
					<tr>
						<td>Id</td>
						<td>Name</td>
						<td>Description</td>
						<td>ECTS</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>{this.props.params.courseId}</td>
						<td>{this.state.course.name}</td>
						<td>{this.state.course.description}</td>
						<td>{this.state.course.ects}</td>
					</tr>
				</tbody>
			</table>
		)
	}
});

/// /= = = = = = = = [PROFESSOR] 
var Professor = React.createClass ({
	getInitialState: function() {
		return {
			/*professor: {
				"details":{
					"id":1423,
					"name":"Duarte Andorinha TESTE",
					"age":21,
					"email":"d.andorinha@fct.unl.pt",
					"adress":"Rua das Francesinhas, numero 21 4o esq",
					"photoUrl":"http://www.newhdwallpaper.in/wp-content/uploads/2014/09/Happy-college-student-nice-pose.jpg"
				},
				"managedCourses":[
					{"id":"IP2012","name":"Introducao Programacao TESTE","listStudents":[
						{"id":3254,"name":"Francisco Urso","grade":13},
						{"id":3221,"name":"Leonardo Tartaruga","grade":11},
						{"id":4563,"name":"Miguel Aguia","grade":15}
						]
					},
					{"id":"CP2016","name":"Concorrencia e Paralelismo","listStudents":[
						{"id":4321,"name":"Joao Abelha TESTE","grade":17},
						{"id":3333,"name":"Bernardo Mosquito","grade":12}
						]
					},
					{"id":"CI2015","name":"CIAI","listStudents":[
						{"id":3254,"name":"Francisco Urso","grade":16},
						{"id":5423,"name":"Ricardo Pavao","grade":9},
						{"id":2399,"name":"Catarina Formiga TESTE","grade":15}
						]
					}
				]
			}*/
			details: {},
			/*managedCourses: [
					{"id":"IP2012","name":"Introducao Programacao TESTE","listStudents":[
						{"id":3254,"name":"Francisco Urso","grade":13},
						{"id":3221,"name":"Leonardo Tartaruga","grade":11},
						{"id":4563,"name":"Miguel Aguia","grade":15}
						]
					},
					{"id":"CP2016","name":"Concorrencia e Paralelismo","listStudents":[
						{"id":4321,"name":"Joao Abelha TESTE","grade":17},
						{"id":3333,"name":"Bernardo Mosquito","grade":12}
						]
					},
					{"id":"CI2015","name":"CIAI","listStudents":[
						{"id":3254,"name":"Francisco Urso","grade":16},
						{"id":5423,"name":"Ricardo Pavao","grade":9},
						{"id":2399,"name":"Catarina Formiga TESTE","grade":15}
						]
					}
			]*/
			managedCourses: {}
		}
	},

	componentDidMount: function() {
		$.ajax({
			type: "GET",
			url: "http://localhost:8080/professor/3",
			success: function(response) {
				this.setState({details: response});
				this.setState({managedCourses: response.courses});
			}.bind(this),
			error: function(e) {
				console.log("error", e);
			}
		});
	},

	componentWillReceiveProps: function() {
		$.ajax({
			type: "GET",
			url: "http://localhost:8080/professor/3",
			success: function(response) {
				this.setState({details: response});
				this.setState({managedCourses: response.courses});
			}.bind(this),
			error: function(e) {
				console.log("error", e);
			}
		});
	},


	render: function() {
		if (Object.getOwnPropertyNames(this.state.details).length === 0) { //If is empty
			return false;
		} else {
			return (
				<section className="professor">
					<ProfessorDetails details={this.state.details} />
					<ProfessorManagedCourses managedCourses={this.state.managedCourses} />
				</section>
			);
		}
	}
});



/// /ProfessorDetails
var ProfessorDetails = React.createClass ({

	render: function() {
		return(
			<div>
				<div className="personal container row">
					<img src={this.props.details.photoUrl} className='img-responsive img-circle col-sm-4'/>	
					<table className='col-sm-8 table-striped info'>
						<tbody>
							<tr>
								<td><b>Name:</b></td>
								<td>{this.props.details.name}</td>
							</tr>
							<tr>
								<td><b>Age:</b></td>
								<td>{this.props.details.age}</td>
							</tr>
							<tr>
								<td><b>Email:</b></td>
								<td>{this.props.details.email}</td>
							</tr>
							<tr>
								<td><b>Address:</b></td>
								<td>{this.props.details.adress}</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div className='simple'>
					<Link to={"/prof/edit/".concat(this.props.details.id)}>Edit</Link>
				</div>
			</div>
		);
	}
});

//// ProfessorForm 
var ProfessorForm = React.createClass ({
	contextTypes: {
		router: React.PropTypes.object.isRequired
	},


	getInitialState: function() {
		return {
			id:-1,
			name: "",
			age: -1,
			email: "",
			adress: "",
			photoUrl: ""
		}
	},

	componentDidMount: function() {
		$.ajax({
			type: "GET",
			url: "http://localhost:8080/professor/3",
			success: function(response) {
				this.setState({id: response.id});
				this.setState({name: response.name});
				this.setState({age: response.age});
				this.setState({email: response.email});
				this.setState({adress: response.adress});
				this.setState({photoUrl: response.photoUrl});
			}.bind(this),
			error: function(e) {
				console.log("error", e);
			}
		});
	},

	handleChangeName: function(e) {
		this.setState({name: e.target.value});
	},

	handleChangeAge: function(e) {
		this.setState({age: e.target.value});
	},

	handleChangeEmail: function(e) {
		this.setState({email: e.target.value});
	},

	handleChangeAdress: function(e) {
		this.setState({adress: e.target.value});
	},

	handleChangePhoto: function(e) {
		this.setState({photoUrl: e.target.value});
	},

	handleSubmit: function(e) {
		e.preventDefault();

		var editedProf = {
			id: this.state.id,
			name: this.state.name,
			age: this.state.age,
			email: this.state.email,
			adress: this.state.adress,
			photoUrl: this.state.photoUrl,
			courses: []
		}

		//							 id do fieldset da form professorDetails 			 
		// var formData = JSON.stringify($("#profDetails").serializeArray());
		
		/*var formData = {
		 			"id":null,
					"name":"Elvis",
					"age":23,
					"email":"d.andorinha@fct.unl.pt",
					"adress":"Rua das Francesinhas, numero 52 4o esq",
					"photoUrl":"http://www.newhdwallpaper.in/wp-content/uploads/2014/09/Happy-college-student-nice-pose.jpg"
					}*/
							
		
		$.ajax({
			dataType: "json",
		  	contentType : "application/json",
			type: "PUT",
			url: "/professor/edit",
			//data: JSON.stringify(formData),		/*JSON.stringify(this.state.editedProf), */
			data: JSON.stringify(editedProf),
			success: function(){
				console.log("It worked");
			},
			error: function (e) {
				console.log("error", e);
			}
		});

		//this.context.router.replace("professor");
		//browserHistory.push('professor')
	},

	render: function() {
		return (
			<div className='simple'>
				<div className="row">
					<section className="col-md-12">
						<img src={this.state.photoUrl} className="img-responsive img-circle avatar-img profImg"/>
						<form onSubmit={this.handleSubmit} action="">
							<fieldset id="profDetails">
								<legend>Personal Information</legend>
								<table className='table simple-table'>
									<tbody>
										<tr>
											<td><label htmlFor="name">Name:</label></td>
											<td><input type="text" name="name" 
												value={this.state.name} 
											onChange={this.handleChangeName} /></td>
										</tr>
										<tr>
											<td><label htmlFor="age">Age:</label></td>
											<td><input type="text" name="age" 
												value={this.state.age} 
											onChange={this.handleChangeAge} /></td>
										</tr>
										<tr>
											<td><label htmlFor="email">Email:</label></td>
											<td><input type="text" name="email" 
												value={this.state.email} 
											onChange={this.handleChangeEmail} /></td>
										</tr>
										<tr>
											<td><label htmlFor="adress">Address:</label></td>
											<td><input type="text" name="adress" 
												value={this.state.adress} 
											onChange={this.handleChangeAdress} /></td>
										</tr>
										<tr>
											<td><label htmlFor="photoUrl">PhotoUrl:</label></td>
											<td><input type="text" name="photoUrl" 
												value={this.state.photoUrl} 
											onChange={this.handleChangePhoto} /></td>
										</tr>
									</tbody>
								</table>
								<button type="submit" value="Save" 
									className="btn btn-default">Save</button>
							</fieldset>
						</form>
					</section>
				</div>
			</div>
		);
	}
});



// ProfessorSelectedCourse 
var ProfessorSelectedCourse = React.createClass ({
	render: function() {
		if (Object.getOwnPropertyNames(this.props.selectedCourse).length === 0) { //If is emtpy																			// empty
			return false;
		} else {
			return (
				<div>
					<table className="table table-striped table-responsive">
						<caption>{this.props.selectedCourse.name}</caption>
						<thead>
							<tr>
								<th>StudentName</th>
								<th>Grade</th>
							</tr>
						</thead>
						<tbody>
							{this.props.selectedCourse.enrollments.map(enrollment => (
								<tr key={enrollment.id}>
									<td>{enrollment.student.name}</td>
									<td>{enrollment.avaliation}</td>
								</tr>
							))}
						</tbody>
					</table>
					<div className="simple">
						<Link to={"/course/edit/".concat(this.props.selectedCourse.id)}>Edit</Link>
					</div>
				</div>
			);
		}
	}
});




//ProfessorManagedCourses 
var ProfessorManagedCourses = React.createClass ({
	getInitialState: function() {
		return {
			selectedCourse: {}
		}
	},

	componentWillReceiveProps: function(nextProps) {
		this.setState({selectedCourse: nextProps.managedCourses[0]});
	},

	handleChange: function(e) {
		var managedCourses = this.props.managedCourses;
		var pos = -1;

		for (var i = 0; i < managedCourses.length; i++) {
			if (managedCourses[i].id == e.target.value) {
				pos = i;
				break;
			}
		}

		this.setState({selectedCourse: this.props.managedCourses[pos]});
	},

	render: function() {
		if (Object.getOwnPropertyNames(this.props.managedCourses).length === 0) { //If is empty
			return false;
		} else {
			return (
				<div className="row simple">
					<section className="">
						<div className="form-group">
							<label>Select desired course</label>
							<select className="form-group" onChange={this.handleChange}>
								{this.props.managedCourses.map(course => (
									<option value={course.id} key={course.id}>{course.name}</option>
								))}
							</select>
						</div>
						<ProfessorSelectedCourse selectedCourse={this.state.selectedCourse} />
					</section>
				</div>
			);
		}
	}
});


//ProfessorManageGrades 
var ProfessorManageGrades = React.createClass ({
	getInitialState: function() {
		return {
			listStudents: []
		}
	},

	componentWillReceiveProps: function(nextProps) {
		$.ajax({
			type: "GET",
			url: "http://localhost:8080/courses/editGrades/".concat(nextProps.courseId),
			success: function(response) {
				this.setState({listStudents: response});
			}.bind(this),
			error: function(e) {
				console.log("error", e);
			}
		});
	},

	handleChange: function(e) {
		e.preventDefault();
		var pos = -1;
		var value = e.target.value;
		for (var i = 0; i < this.state.listStudents.length; i++) {
			if (this.state.listStudents[i].id == e.target.id) {
				pos = i;
				break;
			}
		}

		this.setState(
			function(prevState) {
				var newList = prevState.listStudents
				newList[pos].avaliation = value;
				return {listStudents: newList}
			}
		);
	},

	handleAddStudent: function(newStudent) {
		this.setState(
			function(prevState) {
				var newList = prevState.listStudents
				newList.push(newStudent);
				return {listStudents: newList}
			}
		);
	},

	handleClick: function(e) {
		e.preventDefault();
		var pos = -1;
		var id = e.target.value;

		for (var i = 0; i < this.state.listStudents.length; i++) {
			if (this.state.listStudents[i].id == id) {
				pos = i;
				break;
			}
		}

		this.setState(
			function(prevState) {
				var newList = prevState.listStudents
				newList.splice(pos, 1);
				return {listStudents: newList}
			}
		);
	},

	handleSubmit: function(e) {
		e.preventDefault();

		var editedGrades = [
		]

		this.state.listStudents.map(enrollment => (
			editedGrades.push(enrollment)
		));

		$.ajax({
			dataType: "json",
		  	contentType : "application/json",
			type: "PUT",
			url: "http://localhost:8080/courses/editGrades",
			//data: JSON.stringify(formData),		/*JSON.stringify(this.state.editedProf), */
			data: JSON.stringify(editedGrades),
			success: function(){
				console.log("It worked");
			},
			error: function (e) {
				console.log("error", e);
			}
		});
	},

	render: function() {
		return (
			<div className="row simple">
				<hr />
				<section>
					<table className="table table-striped table-responsive">
						<caption>List of students and their grades</caption>
						<thead>
							<tr>
								<th>StudentName</th>
								<th>Grade</th>
							</tr>
						</thead>
						<tbody>
							{this.state.listStudents.map(enrollment => (
								<tr key={enrollment.id}>
									<td>{enrollment.student.name}</td>
									<td><input type="text" key={enrollment.id} id={enrollment.id}
										onChange={this.handleChange} value={enrollment.avaliation}/></td>
									<td><button value={enrollment.id} onClick={this.handleClick}>Remove Student</button></td>
								</tr>
							))}
						</tbody>
					</table>
				</section>
				<button className="centered" onClick={this.handleSubmit}>Save changes</button>
				<ProfessorAddStudent addStudent={this.handleAddStudent} />
			</div>
		);
	}
});



//ProfessorAddStudent 
var ProfessorAddStudent = React.createClass ({
	getInitialState: function() {
		return {
			studentName: "",
			id: 0
		}
	},

	handleChange: function(e) {
		this.setState({studentName: e.target.value});
	},

	handleClick: function(e) {
		e.preventDefault();

		if (this.state.studentName != "") {
			this.setState(
				function(prevState) {
					var newId = prevState.id;
					newId++;
					return {id: newId}
				}
			);

			var newStudent = {id: this.state.id, name: this.state.studentName, grade: 0};

			this.setState({studentName: ""});

			this.props.addStudent(newStudent);
		}
	},

	render: function() {
		return (
			<section className="col-md-6">
				<h5>Add student name</h5>
				<input type="text" onChange={this.handleChange} value={this.state.studentName} />
				<button onClick={this.handleClick}>Add</button>
			</section>
		);
	}
});



//ProfessorEditCourse
var ProfessorEditCourse = React.createClass ({
	contextTypes: {
		router: React.PropTypes.object.isRequired
	},


	getInitialState: function() {
		return {
			id: -1,
			name: "test",
			description: "test",
			ects: -1
		}
	},

	componentDidMount: function() {
		$.ajax({
			type: "GET",
			url: "http://localhost:8080/courses//profEdit/".concat(this.props.params.id),
			success: function(response) {
				this.setState({id: response.id});
				this.setState({name: response.name});
				this.setState({description: response.description});
				this.setState({ects: response.ects});
			}.bind(this),
			error: function(e) {
				console.log("error", e);
			}
		});
	},

	handleChangeName: function(e) {
		this.setState({name: e.target.value});
	},

	handleChangeDescription: function(e) {
		this.setState({description: e.target.value});
	},

	handleChangeECTS: function(e) {
		this.setState({ects: e.target.value});
	},

	handleClick: function(e) {
		e.preventDefault();

		var editedCourse = [this.state.id.toString(), this.state.name.toString(), this.state.description.toString(), this.state.ects.toString()];
		$.ajax({
			dataType: "json",
		  	contentType : "application/json",
			type: "PUT",
			url: "http://localhost:8080/courses/profEdit/",
			//data: JSON.stringify(formData),		/*JSON.stringify(this.state.editedProf), */
			data: JSON.stringify(editedCourse),
			success: function(){
				console.log("It worked");
			},
			error: function (e) {
				console.log("error", e);
			}
		});

		//this.context.router.replace("professor");
	},

	render: function() {
		return (
			<section className="editProf">
				<br />
				<div className="row">
					<section>
						<form onSubmit={this.handleSubmit} action="">
							<table className='simple-table table table-responsive table-striped'>
								<tbody>
									<tr>
										<td><label htmlFor="name">Name</label></td>
										<td><input type="text" onChange={this.handleChangeName} 
										value={this.state.name} /></td>
									</tr>
									<tr>
										<td><label htmlFor="description">Description</label></td>
										<td><textarea rows='4' cols='80' onChange={this.handleChangeDescription} 
										value={this.state.description} /></td>
									</tr>
									<tr>
										<td><label htmlFor="ects">ECTS</label></td>
										<td><input type="text" onChange={this.handleChangeECTS} 
										value={this.state.ects} /></td>
									</tr>
								</tbody>
							</table>
						</form>
					</section>
				</div>
				<button className="centered" onClick={this.handleClick}>Save changes</button>
				<ProfessorManageGrades courseId={this.state.id} />
			</section>
		);
	}
});

//*// */ 
ReactDOM.render( 
		<Router>
    		<Route component={MainLayout}>
    			<Route path='/' component={Home} />
    			<Route path='/profile' component={StudentLayout} />
    			<Route path='/degrees' component={DegreeIndex} />
    			<Route path='/courses' component={CourseLayout} />
    			<Route path='/api/courses/:courseId' component={CourseDetails} />
    			<Route path="/professor" component={Professor}/>
    			<Route path="/prof/edit/:id" component={ProfessorForm}/>
    			<Route path="/course/edit/:id" component={ProfessorEditCourse} />
    		</Route>
    	</Router>
		,document.getElementById('react_content'));
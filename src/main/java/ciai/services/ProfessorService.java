package ciai.services;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ciai.model.CourseRepository;
import ciai.model.Professor;
import ciai.model.ProfessorRepository;

@Component
public class ProfessorService {
	@Autowired
	ProfessorRepository professorRepository;
	
	@Autowired
	CourseRepository courseRepository;
	
	
	
	@Transactional
	public Professor getProfessor(long id) {
		return professorRepository.findById(id);
	}
	
	@Transactional
	public void setProfessor(Professor p) {
		professorRepository.save(p);
	}
}

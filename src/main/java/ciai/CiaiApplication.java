package ciai;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CiaiApplication {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(CiaiApplication.class);
	
	public static void main(String[] args) {
		SpringApplication.run(CiaiApplication.class, args);
	}
	
}

package ciai;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonView;

import ciai.model.Professor;
import ciai.services.ProfessorService;
import ciai.views.Views;

@Controller
@RequestMapping(value = "/professor")
public class ProfessorController {
	
	@Autowired
	ProfessorService professorService;
											//data from DB
	
	/*@RequestMapping(value = "")
	public @ResponseBody List<Professor> getProfessor() {
		return  Arrays.asList(professor);
	}*/
	
	@RequestMapping(value = "/{id}", method=RequestMethod.GET)
	@JsonView(Views.ProfessorView.class)
	public @ResponseBody Professor getProfessor(@PathVariable long id) {
		return professorService.getProfessor(id);
	}

	@RequestMapping(value = "/edit", method=RequestMethod.PUT)	
	public @ResponseBody Professor setProfessor(@RequestBody Professor professor) {
		professorService.setProfessor(professor);
		return professor;				  
	}
	
		}
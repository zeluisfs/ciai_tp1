package ciai;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ciai.model.Degree;
import ciai.model.DegreeRepository;

@Controller
@RequestMapping(value = "/degrees")
public class DegreeController {

	@Autowired
	DegreeRepository degrees;
	
	@RequestMapping(value = "")
	public @ResponseBody Iterable<Degree> getDegrees() {
		return degrees.findAll();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody Degree getDegree(@PathVariable long id) {
		return degrees.findOne(id);
	}

	@RequestMapping(value= "", method=RequestMethod.POST)
	public @ResponseBody long addStudent(@RequestBody Degree degree) {
		degrees.save(degree);
		return degree.getId();
	}
}

package ciai.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonView;

import ciai.views.Views;

@Entity
public class Professor {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@JsonView(Views.ProfessorView.class)
	private long id;
	
	@JsonView(Views.ProfessorView.class)
	private String name;
	
	@JsonView(Views.ProfessorView.class)
	private int age;
	
	@JsonView(Views.ProfessorView.class)
	private String email;
	
	@JsonView(Views.ProfessorView.class)
	private String adress;
	
	@JsonView(Views.ProfessorView.class)
	private String photoUrl;
	
	@JsonView(Views.ProfessorView.class)
	@OneToMany(mappedBy = "professor", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Course> courses;
	
	
	public Professor(){}
	
	public Professor(String name, int age, String email, String adress, String photoUrl) {
		this.setName(name);
		this.setAge(age);
		this.setEmail(email);
		this.setAdress(adress);
		this.setPhotoUrl(photoUrl);
	}
	
	public Professor(long id, String name, int age, String email, String adress, String photoUrl, Set<Course> courses) {
		this.setId(id);
		this.setName(name);
		this.setAge(age);
		this.setEmail(email);
		this.setAdress(adress);
		this.setPhotoUrl(photoUrl);
		this.setCourses(courses);
	}
	
	
	public long getId() {
		return id;
	}		
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getAge() {
		return age;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getAdress() {
		return adress;
	}
	
	public void setAdress(String adress) {
		this.adress = adress;
	}
	
	public String getPhotoUrl(){
		return photoUrl;
	}
	
	public void setPhotoUrl(String photoUrl){
		this.photoUrl = photoUrl;
	}
	
	public Set<Course> getCourses() {
		return courses;
	}
	
	public void setCourses(Set<Course> courses) {
		this.courses = courses;
	}
} 

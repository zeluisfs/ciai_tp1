package ciai.model;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface CourseRepository extends PagingAndSortingRepository<Course, Long> {
	
	@Query(" select c from Course c "+
	     	   " inner join c.enrollments"+
	     	   " inner join c.professor"+
	     	   " where c.id = :id")
	Course findOne(@Param("id") long id);
	
	List<Course> findByName(String name);
	
	List<Course> findByProfessor(Professor professor);
}

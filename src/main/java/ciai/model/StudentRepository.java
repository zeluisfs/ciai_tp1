package ciai.model;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface StudentRepository extends CrudRepository<Student, Long> {
	@Query(" select s from Student s "+
	     	   " inner join s.enrollments"+
	     	   " where s.id = :id")
	Student findOne(@Param("id") long id);
}

package ciai.model;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface EnrollmentRepository extends PagingAndSortingRepository<Enrollment, Long> {
	
	@Query(" select e from Enrollment e "+
	     	   " inner join e.course"+
	     	   " inner join e.student"+
	     	   " where e.id = :id")
	Enrollment findOne(@Param("id") long id);
	
	Enrollment findById(long id);
}

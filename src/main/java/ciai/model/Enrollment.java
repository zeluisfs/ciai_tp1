package ciai.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonView;

import ciai.views.Views;

@Entity
public class Enrollment {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@JsonView(Views.ProfessorView.class)
	private long id;
	
	@ManyToOne
	@NotNull
	private Course  course;
	
	@ManyToOne
	@NotNull
	@JsonView(Views.ProfessorView.class)
	private Student student;
	
	@JsonView(Views.ProfessorView.class)
	private int avaliation;
	
	public Enrollment() {}
	
	public Enrollment(Course course, Student student, int avaliation) {
		this.course = course;
		this.student = student;
		this.avaliation = avaliation;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public int getAvaliation() {
		return avaliation;
	}

	public void setAvaliation(int avaliation) {
		this.avaliation = avaliation;
	}
	
	
}

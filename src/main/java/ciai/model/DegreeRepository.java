package ciai.model;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface DegreeRepository extends PagingAndSortingRepository<Degree, Long> {
	@Query(" select s from Student s "+
		   " inner join s.degree d"+
		   " where d.id = :id")
	Degree findOne(@Param("id") long id);
}

package ciai.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonView;

import ciai.views.Views;

@Entity
public class Student {

	@JsonView(Views.ProfessorView.class)
	private @Id @GeneratedValue long id;
	
	@JsonView(Views.ProfessorView.class)
	private String name;
	private String email;
	private String address;
	private String photo;
	private @OneToOne Degree degree; //NAO PODE TER (cascade=CascadeType.ALL) n sei se é por o degree n ter associacao pa isto ou outra coisa...
	private @OneToMany(cascade=CascadeType.ALL) Set<Course> enrolled;
	private @OneToMany(cascade=CascadeType.ALL) Set<Course> concluded;
	private @OneToMany(mappedBy = "student", cascade=CascadeType.ALL, fetch = FetchType.EAGER) Set<Enrollment> enrollments;
	
	private Student(){}
	
	public Student(String name, String email, String address, Degree degree){
		this(name, email, address, "images/user.png", degree);
	}

	public Student(String name, String email, String address, String photo, Degree degree){
		this.name = name;
		this.email = email;
		this.address = address;
		this.photo = photo;
		this.degree = degree;
		this.enrolled = null;
		this.concluded = null;
	}
	
	public Set<Course> getEnrolled() {
		return enrolled;
	}

	public void setEnrolled(Set<Course> enrolled) {
		this.enrolled = enrolled;
	}

	public Set<Course> getConcluded() {
		return concluded;
	}

	public void setConcluded(Set<Course> concluded) {
		this.concluded = concluded;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getPhoto() {
		return photo;
	}

	public String getDegree() {
		return degree.toString();
	}

	public void setDegree(Degree degree) {
		this.degree = degree;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Set<Enrollment> getEnrollments() {
		return enrollments;
	}

	public void setEnrollments(Set<Enrollment> enrollments) {
		this.enrollments = enrollments;
	}
}

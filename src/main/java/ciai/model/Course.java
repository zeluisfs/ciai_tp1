package ciai.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonView;

import ciai.views.Views;

@Entity
public class Course {
	
	@JsonView(Views.ProfessorView.class)
	private @Id @GeneratedValue Long id;
	
	@JsonView(Views.ProfessorView.class)
	private String name;
	@JsonView(Views.CourseView.class)
	private String description;
	@JsonView(Views.CourseView.class)
	private int ects; 
	//private Set<Editions>;
	@NotNull
	@ManyToOne
	private Professor professor;
	
	@JsonView(Views.ProfessorView.class)
	@OneToMany(mappedBy = "course", cascade=CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Enrollment> enrollments;
	
	@SuppressWarnings("unused")
	private Course(){}
	
	public Course(String name, String description, int ects, Professor professor){
		this.name = name;
		this.description = description;
		this.ects = ects;
		this.setProfessor(professor);
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getEcts() {
		return ects;
	}

	public void setEcts(int ects) {
		this.ects = ects;
	}
	
	public Professor getProfessor() {
		return professor;
	}
	
	public void setProfessor(Professor professor) {
		this.professor = professor;
	}

	public Set<Enrollment> getEnrollments() {
		return enrollments;
	}

	public void setEnrollments(Set<Enrollment> enrollments) {
		this.enrollments = enrollments;
	}
}

package ciai.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Degree {

	private @Id @GeneratedValue Long id;
	private String name;
	private int years;
	private int ects;
	// private Set<Course> courses;
	
	private Degree(){}
	
	public Degree(String name, int years, int ects){
		this.name = name;
		this.years = years;
		this.ects = ects;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getYears() {
		return years;
	}

	public void setYears(int years) {
		this.years = years;
	}

	public int getEcts() {
		return ects;
	}

	public void setEcts(int ects) {
		this.ects = ects;
	}
	
	@Override
	public String toString(){
		return getName();
	}
	
}

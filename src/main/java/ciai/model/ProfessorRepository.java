package ciai.model;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface ProfessorRepository extends PagingAndSortingRepository<Professor, Long> {
	
	@Query(" select p from Professor p "+
	     	   " inner join p.courses"+
	     	   " where p.id = :id")
	Professor findOne(@Param("id") long id);
	
	Professor findById(long id);
	
	List<Professor> findByName(String name);
	
	@Query("select s from Professor s where s.name like CONCAT(?,'%')")
    List<Professor> search(String name);
}

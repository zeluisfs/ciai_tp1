package ciai;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ciai.model.Student;
import ciai.model.StudentRepository;

@Controller
@RequestMapping(value = "/students")
public class StudentController {

	@Autowired
	StudentRepository students;
	
	@RequestMapping(value = "")
	public @ResponseBody Iterable<Student> getStudents() {
		return students.findAll();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody Student getStudent(@PathVariable long id) {
		return students.findOne(id);
	}

	@RequestMapping(value= "", method=RequestMethod.POST)
	public @ResponseBody long addStudent(@RequestBody Student student) {
		students.save(student);
		return student.getId();
	}
}

package ciai;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import ciai.model.Course;
import ciai.model.CourseRepository;
import ciai.model.Degree;
import ciai.model.DegreeRepository;
import ciai.model.Enrollment;
import ciai.model.EnrollmentRepository;
import ciai.model.Professor;
import ciai.model.ProfessorRepository;
import ciai.model.Student;
import ciai.model.StudentRepository;

@Component
public class DatabaseLoader implements CommandLineRunner {

	private final StudentRepository students;
	private final CourseRepository courses;
	private final DegreeRepository degrees;
	private final ProfessorRepository professorRepository;
	private final EnrollmentRepository enrollmentRepository;

	@Autowired
	public DatabaseLoader(StudentRepository studentRepo, CourseRepository courseRepo, DegreeRepository degreeRepo,
			ProfessorRepository professorRepository, EnrollmentRepository enrollmentRepository) {
		this.students = studentRepo;
		this.courses = courseRepo;
		this.degrees = degreeRepo;
		this.professorRepository = professorRepository;
		this.enrollmentRepository = enrollmentRepository;
	}

	@Override
	public void run(String... strings) throws Exception {
		// Degrees
		Degree[] ds = 
			{new Degree("MIEI",5,300),
			 new Degree("BEAI",3,180)};
		
		for(Degree d: ds) degrees.save(d);
		
		// Professors
		Professor[] profs = {new Professor("Custódio Formiga", 35, "formiga@gmail.com", "Rua da Formiga numero 33 2o andar", "http://www.newhdwallpaper.in/wp-content/uploads/2014/09/Happy-college-student-nice-pose.jpg"), 
				new Professor("Francisco Rato", 35, "rato@gmail.com", "Rua do Rato numero 98", "http://www.newhdwallpaper.in/wp-content/uploads/2014/09/Happy-college-student-nice-pose.jpg"), 
				new Professor("Duarte Rinoceronte", 35, "Rinoceronte@gmail.com", "Rua do Rinoceronte numero 15 3o andar", "http://www.newhdwallpaper.in/wp-content/uploads/2014/09/Happy-college-student-nice-pose.jpg"), 
				new Professor("Manuel Mosquito", 35, "mosquito@gmail.com", "Rua do Mosquito numero 12 1o andar", "http://www.newhdwallpaper.in/wp-content/uploads/2014/09/Happy-college-student-nice-pose.jpg"), 
				new Professor("Sebastião Crocodilo", 35, "crocodilo@gmail.com", "Rua do Crocodilo numero 76 5o andar", "http://www.newhdwallpaper.in/wp-content/uploads/2014/09/Happy-college-student-nice-pose.jpg")};
		
		for (Professor p : profs) { //They have to be saved here in order to be saved on the Course when the course saves them
			professorRepository.save(p);
		}
		
		// Courses
				Course[] cs = 
					{new Course("LC","Introduction to Computational Logic",6, profs[0]),
					 new Course("CP","Concurrency and Parallellism",6, profs[1]),
					 new Course("IP","Introduction to Programming",6, profs[2]),
					 new Course("AED","Algoritmos e Estruturas",6, profs[2]),
					 new Course("SD","Distribuiton Systems",6, profs[3]),
					 new Course("RCSR","Logic m8",6, profs[4])};
		
		// Students
		Student[] ss = 
			{new Student("Anthony Jeselnik","@yourmom","ABCD Street", ds[0]),
			 new Student("Bernard Ole","@bernard","Bernard Street", ds[0]),
			 new Student("Custody Degree","@custody","Custody Street", ds[0]),
			 new Student("Diegos Maradonna","@diegos","Diegos Street", ds[0]),
			 new Student("Francis Obikwello","@francis","Francis Street", ds[0]),
			 new Student("Gustaff The Wizard","@gustaff","Gustaff Street", ds[1])};
		
		Set<Course> enrolled = new HashSet<Course>();
		Set<Course> concluded = new HashSet<Course>();
		enrolled.add(cs[0]);
		concluded.add(cs[1]);
		
		ss[0].setConcluded(concluded);
		ss[0].setEnrolled(enrolled);
		
		for(Student s: ss) students.save(s);
		for(Course c: cs) courses.save(c);
		
		//Enrollments
				Enrollment[] es = {
						new Enrollment(cs[0], ss[0], 13),
						new Enrollment(cs[0], ss[1], 12),
						new Enrollment(cs[1], ss[1], 14),
						new Enrollment(cs[1], ss[2], 9),
						new Enrollment(cs[2], ss[2], 12),
						new Enrollment(cs[2], ss[3], 11),
						new Enrollment(cs[3], ss[3], 10),
						new Enrollment(cs[3], ss[4], 13),
						new Enrollment(cs[4], ss[4], 16)};
		
		for(Enrollment e: es) enrollmentRepository.save(e);
		
		
		
		System.out.println("DB LOADER TESTS");
		System.out.println("Enrollments");
		System.out.println("-------------------------------");
		System.out.println(courses.findOne((long) 1).getName());

	}

}

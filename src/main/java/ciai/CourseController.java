package ciai;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonView;

import ciai.model.Course;
import ciai.model.CourseRepository;
import ciai.model.Enrollment;
import ciai.model.EnrollmentRepository;
import ciai.views.Views;

@Controller
@RequestMapping(value = "/courses")
public class CourseController {

	@Autowired
	CourseRepository courses;
	
	@Autowired
	EnrollmentRepository enrollmentRepository;
	
	@RequestMapping(value = "")
	public @ResponseBody Iterable<Course> getDegrees() {
		return courses.findAll();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody Course getDegree(@PathVariable long id) {
		return courses.findOne(id);
	}

	@RequestMapping(value= "", method=RequestMethod.POST)
	public @ResponseBody long addCourse(@RequestBody Course course) {
		courses.save(course);
		return course.getId();
	}
	
	@RequestMapping(value = "/profEdit/{id}", method = RequestMethod.GET)
	@JsonView(Views.ExtendedCourseView.class)
	public @ResponseBody Course getProfCourse(@PathVariable long id) {
		return courses.findOne(id);
	}
	
	@RequestMapping(value = "/profEdit", method = RequestMethod.PUT)
	public @ResponseBody boolean setProfCourse(@RequestBody String[] params) {
		Course course = courses.findOne((long) Integer.parseInt(params[0]));
		
		course.setName(params[1]);
		course.setDescription(params[2]);
		course.setEcts(Integer.parseInt(params[3]));
		
		courses.save(course);
		
		return true;
	}
	
	@RequestMapping(value = "/editGrades/{id}", method = RequestMethod.GET)
	@JsonView(Views.ExtendedCourseView.class)
	public @ResponseBody Set<Enrollment> editGrades(@PathVariable long id) {
		return courses.findOne(id).getEnrollments();
	}
	
	@RequestMapping(value = "/editGrades", method = RequestMethod.PUT)
	public @ResponseBody boolean setProfGrades(@RequestBody Enrollment[] enrollments) {
		for (Enrollment e : enrollments) {
			Enrollment newEnrollment = enrollmentRepository.findById(e.getId());
			newEnrollment.setAvaliation(e.getAvaliation());
			enrollmentRepository.save(newEnrollment);
		}
		
		return true;
	}
}

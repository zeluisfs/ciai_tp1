package ciai.views;

public class Views {
	public interface ProfessorView {}
	public interface CourseView {}
	//public interface StudentView {}
	//public interface EnrollmentView {}
	//public interface ExtendedProfessorView extends ProfessorView, CourseView {}
	public interface ExtendedCourseView extends CourseView, ProfessorView {}
}
